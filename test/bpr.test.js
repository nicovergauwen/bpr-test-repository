const bpr = require('../lib/index')
const assert = require('assert')
describe('User', function () {
  let repo, prId, username, password, options
  beforeEach(() => {
    repo = 'nicovergauwen/bpr-test-repository',
    username = process.env.USER
    password = process.env.PASS
    options = {
      user: username,
      password
    }
  })
  it('GET: user data', async function() {
    this.timeout(0)
    let user = await bpr.getUser(options)
    assert(user.username === "nicovergauwen")
  })
  it('GET: repository', async function() {
     this.timeout(0);
    let repository = await bpr.getRepo(repo, options)
    assert(repository.name === "bpr-test-repository")
  })
  it('POST: Create pull request', async function() {
    this.timeout(0)
    let res = await bpr.create(repo, "Test PR", "This is a test", "test-branch", "master", true, options)
    assert(res.id !== null)
    assert(res.type === "pullrequest")
    assert(res.title === "Test PR")
    prId = res.id
  })
  it('GET: pull request', async function () {
    this.timeout(0)
    let res = await bpr.get(repo, prId, options)
    assert(res.id === prId)
    assert(res.type === "pullrequest")
    assert(res.title === "Test PR")
    console.log(res)
  })
  it('POST: Approve', async function () {
    this.timeout(0)
    let res = await bpr.approve(repo, prId, options)
    console.log(res)
  })
  it('POST: Decline', async function () {
    this.timeout(0)
    let res = await bpr.decline(repo, prId, options)
    console.log(res)
  })
})
